package jira;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static randomorg.TestCases.findAndFill;

public class TestCases {
    static WebDriver browser;


    @BeforeTest
    public static void openBrowser() {
        browser = new ChromeDriver();
    }

    @BeforeMethod
    public static void basePage() {
        browser.get("https://jira.hillel.it/secure/Dashboard.jspa");
    }

    @AfterTest
    public static void closeBrowser() {
        browser.quit();
    }

    @Test
    public static void validLogin (){
        findAndFill(By.cssSelector("input[name='os_username']") , "autorob");
        findAndFill(By.cssSelector("input[name='os_password']") , "forautotests");
        browser.findElement(By.cssSelector("#aui-button aui-button-primary")).click();

        Assert.assertTrue(browser.findElement(By.cssSelector("img[alt='User profile for Automation Robert']")).isDisplayed());
    }

    @Test
    public static void invalidLogin (){
        findAndFill(By.cssSelector("input[name='os_username']") , "autorod");
        findAndFill(By.cssSelector("input[name='os_password']") , "forautotest");
        browser.findElement(By.cssSelector("#aui-button aui-button-primary")).click();

        Assert.assertTrue(browser.findElement(By.cssSelector(".usernameerror")).isDisplayed());
    }

    @Test
    public static void createIssue (){

    }

    @Test
    public static void viewIssue (){

    }

    @Test
    public static void addUser (){

    }

    public static WebElement findAndFill(By locator, String text) {
        WebElement e = browser.findElement(locator);
        e.clear();
        e.sendKeys(text);

        return e;
    }


}
